### The mini tool to manage tmux sessions for projects

usage:

```sh
tmuxto [action] [project] [repository]
```

actions:

```sh
create [project] [repository] # create new project or add your project's repository to tmuxto's manager
start [project] [repository] # start your project fully or specific part (repository)
attach [project] [repository] # attach to root session of project or to specific part
kill [project] [repository] # kill project fully or specific part of project
remove [project] [repository] # remove project fully or specific part of project
active [project] # show all active projects or all parts for specific project
projects # show all projects
current [instace] # show info about current connection
                  # instance can be 'project', 'repo' or empty to show specific info
```

tmux configs to manage sessions store in $HOME/.tmux directory and save in following formats:

- `tmux-{project}.sh` - script, that running when start project fully
- `tmux-{project}.conf` - tmux config for root session
- `tmux-{project}-{repository}.conf` - tmux config for session of specific repository
